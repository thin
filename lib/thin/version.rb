module Thin
  module VERSION #:nodoc:
    MAJOR    = 0
    MINOR    = 5
    TINY     = 5
    
    STRING   = [MAJOR, MINOR, TINY].join('.')
    
    CODENAME = 'Pony'
  end
end
